import { resolve } from 'path'
import { defineConfig, bytecodePlugin, externalizeDepsPlugin } from 'electron-vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig({
  main: {
    // build: {
    //   rollupOptions: {
    //     // external: ['ffi-napi']
    //     input: {
    //       index: resolve(__dirname, 'electron/main/index.ts')
    //     }
    //   },
    // },
    plugins: [
      bytecodePlugin(),
      externalizeDepsPlugin()
    ]
  },
  preload: {
    plugins: [
      bytecodePlugin(),
      externalizeDepsPlugin()
    ]
  },
  renderer: {
    resolve: {
      alias: {
        '@renderer': resolve('src/renderer/src')
      }
    },
    plugins: [vue()]
  }
})
