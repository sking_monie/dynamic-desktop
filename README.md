# dynamicdesktop

Electron+ThreeJS制作的桌面屏保



### Install

```bash
$ npm install
/
$ yarn

```

### Development

```bash
$ npm run dev
/
$ yarn run dev
```

### Build

```bash
# For windows
$ npm run build:win
/
$ yarn run build:win


# For macOS
$ npm run build:mac
/
$ yarn run build:mac

# For Linux
$ npm run build:linux
/
$ yarn run build:linux
```

#### UPDATE

* 2023/01/09

  init start
  add threeJS