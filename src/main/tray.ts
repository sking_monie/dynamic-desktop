import { Tray, Menu, app, BrowserWindow } from 'electron'
import { join } from 'path'
// 任务栏图标
export class TrayIcon {
  trayData: any
  args = []
  quit: any
  constructor() {
    // 初始化任务栏图标
    this.trayData = new Tray(join(__dirname, '../../public/icon.ico'))
    this.trayData.setContextMenu(this.contextMenu(this.args))
    this.initEvent()
  }
  // 创建任务栏图标菜单
  contextMenu(args?: any) {
    const quitFn = this.quit
    const defaultMenu = [
      {
        label: '显示面板', click: () => this.showWindow(),
      },
      {
        // 点击退出菜单退出程序
        label: '退出', click: typeof quitFn === 'function' ? quitFn() : () => {
          app.quit()
        }
      }]
    const contextMenu = Menu.buildFromTemplate([
      ...defaultMenu,
      ...args
    ])
    return contextMenu
  }
  setToolTip() {
    this.trayData?.setToolTip()
  }
  showWindow() {
    const wins = BrowserWindow.getAllWindows()
    wins.map(win => win.show())
  }
  initEvent() {
    this.trayData.on('click', () => this.showWindow())
    this.trayData.on('right-click', () => this.trayData.setContextMenu(this.contextMenu(this.args)))
  }
}