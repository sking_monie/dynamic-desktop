import { ipcMain } from "electron"
import log from 'electron-log'

export const initHandle = () => {
  ipcMain.handle('log-info', (_e,data) => {
    log.info('VIEW MESSAGE：',data)
  })
}