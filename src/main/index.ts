import { app, shell, BrowserWindow, Menu, globalShortcut, webContents, screen } from 'electron'
import * as path from 'path'
import { electronApp, optimizer, is } from '@electron-toolkit/utils'
import log from 'electron-log'
const embed = require('../../public/embed-desktop')
// const embed = require('../../build/embed-desktop')
import { TrayIcon } from './tray'
import { initHandle } from './handle'

const config = {

  show: false,
  autoHideMenuBar: true,
  transparent: true,
  ...(process.platform === 'linux'
    ? {
      icon: path.join(__dirname, '../../build/icon.png')
    }
    : {}),
  webPreferences: {
    preload: path.join(__dirname, '../preload/index.js'),
    sandbox: false
  },
  frame: false, // 无边框窗口
  resizable: false, // 禁止调整窗口大小
  movable: false, // 禁止移动窗口
  skipTaskbar: true, // 禁止在任务栏中显示窗口
}
function createWindow(): void {
  // Create the browser window.
  const primaryDisplay = screen.getPrimaryDisplay()
  const { width, height } = primaryDisplay.workAreaSize

  const mainWindow = new BrowserWindow({
    width: width || 900,
    height: height || 670,
    ...config,
  })
  Menu.setApplicationMenu(null)
  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
    // 打开控制台
    // mainWindow.webContents.openDevTools()
    try {
      let hwnd = mainWindow.getNativeWindowHandle() //获取窗口句柄。
      // log.info(embed)
      embed.embed(hwnd)
    } catch (error) {
      log.info(error, 'error')
    }
  })

  mainWindow.webContents.setWindowOpenHandler((details) => {
    shell.openExternal(details.url)
    return { action: 'deny' }
  })

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
  if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
    mainWindow.loadURL(process.env['ELECTRON_RENDERER_URL'])
  } else {
    mainWindow.loadFile(path.join(__dirname, '../renderer/index.html'))
  }
  // 全局注册打开控制器

  globalShortcut.register('CmdOrCtrl+Alt+F4', () => {
    const content = webContents.getFocusedWebContents()
    return content?.openDevTools()
  })
  // set Always top
  // mainWindow.setAlwaysOnTop(true, 'floating', -1)
}
function createWindowOther(): void {
  // Create the browser window.
  const primaryDisplay = screen.getPrimaryDisplay()
  const { width, height } = primaryDisplay.workAreaSize
  const displays = screen.getAllDisplays()
  const externalDisplay = displays.find((display) => {
    return display.bounds.x !== 0 || display.bounds.y !== 0
  })
  let mainWindow
  if (externalDisplay) {
    mainWindow = new BrowserWindow({
      width: width || 900,
      height: height || 670,
      x: externalDisplay.bounds.x,
      y: externalDisplay.bounds.y,
      ...config,
    })
  }
  Menu.setApplicationMenu(null)
  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
    try {
      let hwnd = mainWindow.getNativeWindowHandle() //获取窗口句柄。
      // log.info(embed)
      embed.embed(hwnd)
    } catch (error) {
      log.info(error, 'error')
    }
  })

  mainWindow.webContents.setWindowOpenHandler((details) => {
    shell.openExternal(details.url)
    return { action: 'deny' }
  })

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
  if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
    mainWindow.loadURL(process.env['ELECTRON_RENDERER_URL'])
  } else {
    mainWindow.loadFile(path.join(__dirname, '../renderer/index.html'))
  }
  // 全局注册打开控制器

  globalShortcut.register('CmdOrCtrl+Alt+F3', () => {
    const content = webContents.getFocusedWebContents()
    return content?.openDevTools()
  })
  // set Always top
  // mainWindow.setAlwaysOnTop(true, 'screen-save')
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  // Set app user model id for windows
  electronApp.setAppUserModelId('com.dynamic.desktop')

  // Default open or close DevTools by F12 in development
  // and ignore CommandOrControl + R in production.
  // see https://github.com/alex8088/electron-toolkit/tree/master/packages/utils
  app.on('browser-window-created', (_, window) => {
    optimizer.watchWindowShortcuts(window)
  })

  createWindow()
  createWindowOther()
  // 获取可执行文件位置ex：在程序打包之前，找到的是node_modules\electron\dist\electron.exe，程序打包安装后，找到的是真正的应用的可执行文件。
  const ex = process.execPath;
  // 监听设置开机自启动
  app.setLoginItemSettings({
    openAtLogin: true,
    openAsHidden: false,
    path: ex,
    args: ["--openAsHidden"],
  })

  // 添加右下角任务栏图标
  new TrayIcon()

  // ipc通信
  initHandle()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('quit', () => {
  log.info('quit')
  embed.refresh()
})

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
